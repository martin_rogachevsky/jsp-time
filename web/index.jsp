<%--
  Created by IntelliJ IDEA.
  User: Martin Rogachevsky
  Date: 5/10/2018
  Time: 4:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
  <head>
    <title>Time</title>
  </head>
  <body>
  <jsp:useBean id="now" class="java.util.Date" scope="request" />
  <fmt:formatDate type="time" value="${now}" />
  </body>
</html>
